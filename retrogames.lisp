(defpackage :retro-games
  (:use :cl :cl-who :hunchentoot :parenscript :cl-mongo))

(defvar *games* '())
(defparameter *game-collection* "game")

(defclass game ()
  ((name :reader name
         :initarg :name)
   (votes :accessor votes
          :initarg :votes
          :initform 0)
   (category :accessor category
             :initarg :category)))

(defmethod print-object ((object game) stream)
  (print-unreadable-object (object stream :type t)
    (with-slots (name votes) object
      (format stream "name: ~s with ~d votes" name votes))))

(defmethod vote-for :after (game)
  (let ((game-doc (game->doc game)))
    (db.update *game-collection* ($ "name" (name game)) game-doc)))

(defmacro standard-page ((&key title script) &body body)
  `(with-html-output-to-string
       (*standard-output* nil :prologue t :indent t)
     (:html :lang "en"
            (:head
             (:meta :charset "utf-8")
             (:title ,title)
             (:link :type "text/css"
                    :rel "stylesheet"
                    :href "/retro.css")
             ,(when script
                    `(:script :type "text/javascript"
                              (str ,script))))
            (:body
             (:div :id "header"
                   (:img :src "/logo.jpg"
                         :alt "Commodore 64"
                         :class "logo")
                   (:span :class "strapline"
                          "Vota por tu juego preferido"))
             ,@body))))
  

;; (defun game-from-name (name)
;;   (find name *games* :test #'string-equal
;;         :key #'name))
(defun game-from-name (name)
  (let ((found-games (docs (db.find *game-collection* ($ "name" name)))))
    (when found-games
      (doc->game (first found-games)))))

(defun game-stored? (game-name)
  (game-from-name game-name))

(defun doc->game (game-doc)
  (make-instance 'game :name (get-element "name" game-doc)
                 :category (get-element "category" game-doc)
                 :votes (get-element "votes" game-doc)))

(defun game->doc (game)
  (with-slots (name votes category) game
  ($ ($ "name" name)
     ($ "votes" votes)
     ($ "category" category))))

;; (defun games ()
;;   (sort (copy-list *games*)
;;         #'>
;;         :key #'votes))
(defun games ()
  (mapcar #'doc->game
          (docs
           (iter
            (db.sort *game-collection*
                     :all
                     :field "votes"
                     :asc nil)))))

(defun unique-index-on (field)
  (db.ensure-index *game-collection*
                   ($ field 1)
                   :unique t))

;; (defun add-game (name)
;;   (unless (game-stored? name)
;;     (push (make-instance 'game :name name) *games*)))
(defun add-game (name category)
  (let ((game (make-instance 'game :name name :category category )))
    (db.insert *game-collection* (game->doc game))))

(defmethod vote-for (game)
  (incf (votes game)))

;; hunchentoot
;; (start (make-instance 'easy-acceptor :port 9080))
(defun start-server (port)
  (start (make-instance 'easy-acceptor :port port)))

;;(push (create-prefix-dispatcher "/retro-games.html"
;;                                'retro-games)
;;      *dispatch-table*)


(define-easy-handler (retro-games :uri "/retro-games") ()
  (standard-page (:title "Top Retro Games")
    (:h1 "Vota por tu juego favorito!")
    (:p "Falta algún juego? Agregalo "
        (:a :href "new-game" "aca"))
    (:h2 "Posiciones")
    (:div :id "chart"
          (:ol
           (dolist (game (games))
             (htm
              (:li (:a :href (format nil "vote?name=~a"
                                     (url-encode (name game))) "Vota!")
                   (fmt "~A con ~d votos" (escape-string (name game))
                        (votes game)))))))))

(define-easy-handler (vote :uri "/vote") (name)
  (when (game-stored? name)
    (vote-for (game-from-name name)))
  (redirect "/retro-games"))

(define-easy-handler (new-game :uri "/new-game") ()
  (standard-page
      (:title "Agrega un juego"
              :script (ps
                        (defvar add-form nil)

                        (defun validate-game-name (evt)
                          (when (= (@ add-form name value) "")
                            (chain evt (prevent-default))
                            (alert "El nombre no puede ser vacio")))

                        (defun init ()
                          (setf add-form (chain document (get-element-by-id "addform")))
                          (chain add-form (add-event-listener "submit" validate-game-name false)))
                        (setf (chain window onload) init)))
    (:h1 "Agregar un juego a la lista")
    (:form :action "/game-added" :method "post" :id "addform"
           (:p "Nombre del juego" (:br)
               (:input :type "text" :name "name" :class "txt"))
           (:p "Categoria" (:br)
               (:input :type "text" :name "category" :class "txt"))
           (:p (:input :type "submit" :value "Agregar" :class "btn")))))

(define-easy-handler (game-added :uri "/game-added") (name category)
  (unless (or (null name) (zerop (length name)))
    (add-game name category))
  (redirect "/retro-games"))

;; mapreduce
(defjs map_category() (emit this.category 1))

(defjs sum_games(c vals)
  (return ((@ Array sum vals))))

;; cl-mongo se rompio, parece
;; (defun sum-by-category ()
;;                (pp (mr.p ($map-reduce "game" map_category sum_games))))

(defun sum-by-category ()
  (pp (mr.p ($map-reduce "game"
                       "function (x) { return emit(this.category, 1);};"
                       "function (c, vals) { return Array.sum(vals);};"))))
